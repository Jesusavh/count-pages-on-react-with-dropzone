import React, { useState } from 'react';
import * as pdfjs from 'pdfjs-dist/build/pdf';

const PdfPageCounter = () => {
  const [numPages, setNumPages] = useState(null);

  const countPages = async (file) => {
    console.log(file)
    try {
      const loadingTask = pdfjs.getDocument(file);
      const pdf = await loadingTask.promise;
      setNumPages(pdf.numPages);
    } catch (error) {
      console.error('Error loading PDF: ', error);
    }
  };

  const handleFileChange = (event) => {
    const file = event.target.files[0];
    if (file) {
      countPages(file);
    }
  };

  return (
    <div>
      <h2>PDF Page Counter</h2>
      <input type="file" onChange={handleFileChange} />
      {numPages !== null && (
        <p>Number of Pages: {numPages}</p>
      )}
    </div>
  );
};

export default PdfPageCounter;