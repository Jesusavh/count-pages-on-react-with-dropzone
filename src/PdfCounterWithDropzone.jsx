import React, { useState } from 'react';
import { Document, Page } from '@react-pdf/renderer';
import { useDropzone } from 'react-dropzone';
//import * as pdfjs from 'pdfjs-dist';
import { pdfjs } from "react-pdf";
pdfjs.GlobalWorkerOptions.workerSrc = `//unpkg.com/pdfjs-dist@${pdfjs.version}/legacy/build/pdf.worker.min.js`;

const PdfPageCounterWithDropzone = () => {
  const [numPages, setNumPages] = useState(null);
  const [file, setFile] = useState(null);
  
  const onDrop = (acceptedFiles) => {
    const file = acceptedFiles[0];
    setFile(file);
    countPages(file);
    console.log(file)
  };
  
  const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop });

  const countPages = async (file) => {
    try {
        const reader = new FileReader();
    reader.onload = async () => { 
        const buffer = reader.result;
        const loadingTask = pdfjs.getDocument(buffer);
        const pdf = await loadingTask.promise;
        console.log(pdf)
        setNumPages(pdf.numPages);
    }
    reader.readAsArrayBuffer(file);
    } catch (error) {
      console.error('Error loading PDF: ', error);
    }
  };

  return (
    <div>
      <h2>PDF Page Counter with Dropzone</h2>
      <div {...getRootProps()} style={dropzoneStyle}>
        <input {...getInputProps()} />
        {
          isDragActive ?
            <p>Drop the PDF here ...</p> :
            <p>Drag 'n' drop a PDF file here, or click to select one</p>
        }
      </div>
      {numPages !== null && (
        <div>
          <p>Number of Pages: {numPages}</p>
          {/* <Document file={file}>
            {Array.from(new Array(numPages), (el, index) => (
              <Page key={`page_${index + 1}`} pageNumber={index + 1} />
            ))}
          </Document> */}
        </div>
      )}
    </div>
  );
};

const dropzoneStyle = {
  border: '2px dashed #cccccc',
  borderRadius: '4px',
  padding: '20px',
  textAlign: 'center',
  cursor: 'pointer',
  marginTop: '20px'
};

export default PdfPageCounterWithDropzone;