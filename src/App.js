import logo from './logo.svg';
import './App.css';
import PdfPageCounter from './PdfPageCounter';
import PdfPageCounterReact from './PageCounterReact';
import PdfPageCounterWithDropzone from './PdfCounterWithDropzone';
import { MapView } from './Map';


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <MapView />
      </header>
    </div>
  );
}

export default App;
