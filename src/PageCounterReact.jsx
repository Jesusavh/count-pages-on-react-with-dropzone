import React, { useState } from 'react';
import { Document, Page } from '@react-pdf/renderer';

const PdfPageCounterReact = () => {
  const [numPages, setNumPages] = useState(null);
  const [file, setFile] = useState(null);

  const handleFileChange = (event) => {
    const file = event.target.files[0];
    if (file) {
      setFile(file);
      countPages(file);
    }
  };

  const countPages = (file) => {
    const reader = new FileReader();
    reader.onload = () => {
      const buffer = reader.result;
      setNumPages(new Uint8Array(buffer));
      console.log(buffer);
    };
    reader.readAsArrayBuffer(file);
  };

  return (
    <div>
      <h2>PDF Page Counter</h2>
      <input type="file" onChange={handleFileChange} />
      {numPages !== null && (
        <div>
          <p>Number of Pages: {numPages}</p>
          {/* <Document file={file}>
            {Array.from(new Array(numPages.length), (el, index) => (
              <Page key={`page_${index + 1}`} pageNumber={index + 1} />
            ))}
          </Document> */}
        </div>
      )}
    </div>
  );
};

export default PdfPageCounterReact